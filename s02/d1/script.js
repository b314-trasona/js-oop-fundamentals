//console.log('yeah')
//use an object literal to create an object representing auser
//encapsulation
//when we add properties or methods to an object , we are performing encapsulation
//the organization of information (as properties ) and behaviors (as methods) to the object that encapsulates them
//the scope of the encapsulation is the literal {}

let studentOne = {
	name: 'John',
	email: 'john@mail.com',
	grades: [89,84,78,88],

	//methods
		//Add the functionalities available to the student as object mehthods
		//'this' refers to the object encapsulating the method whre 'this' is called
	login(){
		console.log(`${this.email} has logged in `);
	},
	logout(){
		console.log(`${this.email} has logged out`);
	},
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are ${this.grades}`);
	},
	average(){
		let sum = 0
		let ave = 0
		this.grades.forEach((e)=>{
			sum = sum + e
		})

		ave = sum/this.grades.length

		return ave
	},
	isGradePass(){
		let ave = this.average()
		if(ave>=85){
			return true
		} else {
			return false
		}
	},
	willPassWithHonors(){
		let ave = this.average()
		if(ave>=90){
			return true
		} else {
			return false
		}
	}

	//Mini -act
		//create a function that will get the quarterly average of the studentOnes grades

		//create a function that will return true if average grade is >= 85 false otherwise

		//create a fucntion willPasswithHonors that returns true if the student has passed and theeir average is >= 90 return true pr false
}

//log the contents of studentOnes encapsulated information in the console
console.log(`student ones name is ${studentOne.name}`);
console.log(`student ones email is ${studentOne.email}`);
console.log(`student ones quarterly grade is ${studentOne.grades}`);

console.log(studentOne);

